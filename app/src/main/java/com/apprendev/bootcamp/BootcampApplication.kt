package com.apprendev.bootcamp

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class BootcampApplication : Application()
