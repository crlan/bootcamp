package com.apprendev.bootcamp.domain

import com.apprendev.bootcamp.data.configuration.ApiConfiguration

interface ApiConfigurationApi {
    suspend fun getApi(): ApiConfiguration
}


