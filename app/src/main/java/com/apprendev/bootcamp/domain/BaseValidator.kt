package com.apprendev.bootcamp.domain

abstract class BaseValidator : Validator {
    override suspend fun invoke(input: String): Boolean {
        val regex = regex.toRegex()
        return input.matches(regex)
    }
}
