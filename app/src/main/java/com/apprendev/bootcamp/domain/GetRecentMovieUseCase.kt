package com.apprendev.bootcamp.domain

import com.apprendev.bootcamp.data.details.MovieDetails
import com.apprendev.bootcamp.data.popularmovies.TheMovieDbServices
import javax.inject.Inject

class GetRecentMovieUseCase @Inject constructor(
    private val apiServices: TheMovieDbServices,
    private val apiConfigurationApi: ApiConfigurationApi
) {
    suspend operator fun invoke(): MovieDetails {
        val configuration = apiConfigurationApi.getApi()
        val movie = apiServices.getLatestMovie()
        return movie.copy(
            backdrop_path = "${configuration.images.secure_base_url}${configuration.images.still_sizes.last()}${movie.poster_path}"
        )
    }
}
