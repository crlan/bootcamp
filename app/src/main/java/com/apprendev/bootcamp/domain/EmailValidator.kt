package com.apprendev.bootcamp.domain

class EmailValidator(override val regex: String = "^(?!\\s*$)[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$") :
    BaseValidator()


