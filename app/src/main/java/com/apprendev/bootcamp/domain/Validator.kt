package com.apprendev.bootcamp.domain

interface Validator {
    val regex: String
    suspend operator fun invoke(input: String): Boolean
}
