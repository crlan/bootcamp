package com.apprendev.bootcamp.domain

import com.apprendev.bootcamp.data.database.MoviesDao
import com.apprendev.bootcamp.data.database.entities.MovieEntity
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map


class PopularMoviesStreamUseCase(private val moviesDao: MoviesDao) {

    suspend fun invoke(): Flow<List<MovieEntity>> {
        return moviesDao
            .getMoviesByListName(POPULAR_MOVIES_LIST)
            .map { popularMovies ->
                val favoritesList = moviesDao.getFavoritesMovies(FAVORITES_MOVIES_LIST)

                return@map popularMovies.onEach { movieEntity ->
                    movieEntity.isFavorite = favoritesList
                        .none {
                            it.title == movieEntity.title
                        }
                }
            }
    }
}
