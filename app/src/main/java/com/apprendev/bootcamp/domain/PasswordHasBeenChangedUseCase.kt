package com.apprendev.bootcamp.domain

import com.apprendev.bootcamp.R
import com.apprendev.bootcamp.entities.FormInputState

class PasswordHasBeenChangedUseCase(
    private val signInFormPersistence: SignInFormPersistence,
    private val passwordValidator: PasswordValidator,
    private val stringResourceProvider: AndroidStringResourceProvider
) {
    suspend fun invoke(password: String): FormInputState {
        val isPasswordValid = passwordValidator.invoke(password)
        if (isPasswordValid) {
            signInFormPersistence.setEmail(password)
        }

        val errorMessage: String? = if (isPasswordValid) {
            null
        } else {
            stringResourceProvider.getString(R.string.sign_in_form_password_error)
        }


        return FormInputState(isValid = isPasswordValid, errorMessage = errorMessage)
    }
}
