package com.apprendev.bootcamp.domain

import com.apprendev.bootcamp.data.configuration.ApiConfiguration
import com.apprendev.bootcamp.data.popularmovies.TheMovieDbServices
import javax.inject.Inject

class CacheApiConfigurationApi @Inject constructor(private val apiServices: TheMovieDbServices) :
    ApiConfigurationApi {

    private var apiConfiguration: ApiConfiguration? = null

    override suspend fun getApi(): ApiConfiguration {
        if (apiConfiguration == null) {
            apiConfiguration = getApiConfiguration()
        }

        return apiConfiguration ?: throw Exception("Object ApiConfiguration cannot be created.")
    }

    private suspend fun getApiConfiguration(): ApiConfiguration {
        return apiServices.getApiConfiguration()
    }


}
