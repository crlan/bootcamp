package com.apprendev.bootcamp.domain

import com.apprendev.bootcamp.data.configuration.ApiConfiguration
import com.apprendev.bootcamp.data.database.MoviesDao
import com.apprendev.bootcamp.data.database.entities.MovieEntity
import com.apprendev.bootcamp.data.database.entities.MoviesList
import com.apprendev.bootcamp.data.popularmovies.Movie
import com.apprendev.bootcamp.data.popularmovies.PopularMovies
import com.apprendev.bootcamp.data.popularmovies.TheMovieDbServices
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

const val POPULAR_MOVIES_LIST = "popular_movies"
const val FAVORITES_MOVIES_LIST = "favorites_movies"

class FetchPopularMoviesUseCase @Inject constructor(
    private val apiServices: TheMovieDbServices,
    private val apiConfigurationApi: ApiConfigurationApi,
    private val moviesDao: MoviesDao
) {

    private suspend fun getPopularMovies(): PopularMovies {
        return apiServices.getPopularMovies()
    }

    suspend operator fun invoke() {
        setUpPopularList()
        val configuration = apiConfigurationApi.getApi()
        val favoritesList = moviesDao.getFavoritesMovies(FAVORITES_MOVIES_LIST)
        val popularMoviesByNetwork = getPopularMovies()

        popularMoviesByNetwork
            .results
            .map { networkMovie ->
                networkMovie.toMovieEntity(configuration)
            }
            .onEach { movieToInsert ->
                movieToInsert.isFavorite = favoritesList
                    .none {
                        it.title == movieToInsert.title
                    }.not()
            }
            .onEach { movieToInsert ->
                val localMovie =
                    moviesDao.findMovieByTitleInList(movieToInsert.title, POPULAR_MOVIES_LIST)

                if (localMovie == null) {
                    moviesDao.insertUserMovie(movieToInsert)
                } else {
                    moviesDao.updateUserMovie(
                        localMovie.copy(
                            popularity = movieToInsert.popularity,
                            isFavorite = movieToInsert.isFavorite
                        )
                    )
                }
            }
    }

    private suspend fun setUpPopularList() {
        val popularMovieList = moviesDao.getListByName(POPULAR_MOVIES_LIST)

        if (popularMovieList == null) {
            // if popular list doesn't exist we proceed to create
            moviesDao.insertList(
                MoviesList(
                    listName = POPULAR_MOVIES_LIST
                )
            )
        }
    }
}

internal fun Movie.toMovieEntity(configuration: ApiConfiguration) = MovieEntity(
    listName = POPULAR_MOVIES_LIST,
    title = this.title.orEmpty(),
    overview = this.overview.orEmpty(),
    poster_path = "${configuration.images.secure_base_url}${configuration.images.still_sizes.last()}${this.poster_path}",
    backdrop_path = "${configuration.images.secure_base_url}${configuration.images.still_sizes.last()}${this.backdrop_path}",
    popularity = this.popularity.orEmpty(),
    id = this.id.orEmpty()
)

class GetPopularMoviesUseCase(private val moviesDao: MoviesDao) {
    suspend fun invoke(): Flow<List<MovieEntity>> {
        return moviesDao.getUserMovies()
    }
}


