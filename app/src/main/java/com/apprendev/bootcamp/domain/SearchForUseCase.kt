package com.apprendev.bootcamp.domain

import com.apprendev.bootcamp.data.database.entities.MovieEntity
import com.apprendev.bootcamp.data.popularmovies.TheMovieDbServices
import javax.inject.Inject

class SearchForUseCase @Inject constructor(
    private val apiServices: TheMovieDbServices,
    private val apiConfigurationApi: ApiConfigurationApi
) {
    suspend fun invoke(query: String): List<MovieEntity> {
        val configuration = apiConfigurationApi.getApi()
        val searchResults = apiServices.searchInMovieApi(query)

        return searchResults
            .results
            .map { movie ->
                movie.toMovieEntity(configuration)
            }
    }
}
