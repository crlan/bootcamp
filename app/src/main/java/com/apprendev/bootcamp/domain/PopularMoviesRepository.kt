package com.apprendev.bootcamp.domain

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.apprendev.bootcamp.data.cart.CartResponse
import com.apprendev.bootcamp.data.database.MoviesDao
import com.apprendev.bootcamp.data.database.entities.MovieEntity
import com.apprendev.bootcamp.data.popularmovies.TheMovieDbServices
import com.apprendev.bootcamp.di.LocalServices
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject


class PopularMoviesRepository @Inject constructor(
    private val apiServices: TheMovieDbServices,
    private val apiConfiguration: ApiConfigurationApi,
    private val moviesDao: MoviesDao
) {

    fun getPopularMoviesResultStream(): Flow<PagingData<MovieEntity>> {
        return Pager(
            config = PagingConfig(enablePlaceholders = true, pageSize = NETWORK_PAGE_SIZE),
            pagingSourceFactory = {
                PopularMoviesPagingSource(
                    apiServices,
                    apiConfiguration,
                    moviesDao
                )
            }
        ).flow
    }


    companion object {
        private const val NETWORK_PAGE_SIZE = 20
    }
}

class LocalCartRepository @Inject constructor(private val localServices: LocalServices) {
    suspend operator fun invoke(): CartResponse {
        return localServices.getCart()
    }
}



