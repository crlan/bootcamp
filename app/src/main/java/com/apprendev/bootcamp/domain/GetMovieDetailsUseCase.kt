package com.apprendev.bootcamp.domain

import com.apprendev.bootcamp.data.details.MovieDetails
import com.apprendev.bootcamp.data.popularmovies.TheMovieDbServices
import javax.inject.Inject


class GetMovieDetailsUseCase @Inject constructor(
    private val apiServices: TheMovieDbServices,
    private val apiConfigurationApi: ApiConfigurationApi
) {
    suspend operator fun invoke(movieId: String): MovieDetails {
        val configuration = apiConfigurationApi.getApi()
        val movieDetails = getMovieDetails(movieId)

        return movieDetails.copy(
            backdrop_path = "${configuration.images.secure_base_url}${configuration.images.still_sizes.last()}${movieDetails.backdrop_path}"
        )
    }

    private suspend fun getMovieDetails(movieId: String): MovieDetails {
        return apiServices.getMovieDetails(movieId)
    }
}


