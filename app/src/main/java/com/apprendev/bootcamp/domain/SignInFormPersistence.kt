package com.apprendev.bootcamp.domain

import com.apprendev.bootcamp.presentation.fragments.signIn.SignInForm

interface SignInFormPersistence {

    fun getSignInForm(): SignInForm

    fun setEmail(email: String)

    fun setPassword(password: String)
}


