package com.apprendev.bootcamp.domain

class NameValidator(override val regex: String = "^(?!\\s*$)[a-zA-Z0-9_#=!?¿¡,ÑñÁÉÍÓÚáéíóú./()*-@]{3,60}$") :
    BaseValidator()
