package com.apprendev.bootcamp.domain

import androidx.paging.PagingSource
import androidx.paging.PagingState
import androidx.paging.map
import com.apprendev.bootcamp.data.database.MoviesDao
import com.apprendev.bootcamp.data.database.entities.MovieEntity
import com.apprendev.bootcamp.data.popularmovies.PopularMovies
import com.apprendev.bootcamp.data.popularmovies.TheMovieDbServices

private const val POPULAR_MOVIES_STARTING_PAGE_INDEX = 1

class PopularMoviesPagingSource(
    private val apiServices: TheMovieDbServices,
    private val apiConfiguration: ApiConfigurationApi,
    private val moviesDao: MoviesDao
) : PagingSource<Int, MovieEntity>() {

    private lateinit var results: PopularMovies

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, MovieEntity> {
        val page = params.key ?: POPULAR_MOVIES_STARTING_PAGE_INDEX
        val favoritesList = moviesDao.getFavoritesMovies(FAVORITES_MOVIES_LIST)

        return try {
            results = apiServices.getPopularMovies(page)
            val movies = results.results.map {
                val result = it.toMovieEntity(apiConfiguration.getApi())
                result.isFavorite = favoritesList.none { it.title == result.title }.not()
                result
            }

            LoadResult.Page(
                data = movies,
                prevKey = if (page == POPULAR_MOVIES_STARTING_PAGE_INDEX) null else page - 1,
                nextKey = if (page == results.totalPages) null else page + 1
            )
        } catch (exception: Exception) {
            LoadResult.Error(exception)
        }
    }

    override fun getRefreshKey(state: PagingState<Int, MovieEntity>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            val anchorPage = state.closestPageToPosition(anchorPosition)
            anchorPage?.prevKey?.plus(1) ?: anchorPage?.nextKey?.minus(1)
        }
    }

}
