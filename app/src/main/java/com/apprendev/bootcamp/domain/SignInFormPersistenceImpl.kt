package com.apprendev.bootcamp.domain

import com.apprendev.bootcamp.presentation.fragments.signIn.SignInForm

class SignInFormPersistenceImpl : SignInFormPersistence {
    private var form: SignInForm = SignInForm()

    override fun getSignInForm(): SignInForm = form

    override fun setEmail(email: String) {
        form.email = email
    }

    override fun setPassword(password: String) {
        form.password = password
    }
}

