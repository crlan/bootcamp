package com.apprendev.bootcamp.domain

class PasswordValidator(override val regex: String = "^(?!\\s*$)[a-zA-Z0-9_#=!?¿¡,ÑñÁÉÍÓÚáéíóú./()*-@]{1,40}$") :
    BaseValidator()


