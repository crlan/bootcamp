package com.apprendev.bootcamp.domain

import com.apprendev.bootcamp.R
import com.apprendev.bootcamp.entities.FormInputState
import javax.inject.Inject

class EmailHasBeenChangedUseCase @Inject constructor(
    private val signInFormPersistence: SignInFormPersistence,
    private val emailValidator: EmailValidator,
    private val stringResourceProvider: AndroidStringResourceProvider
) {
    suspend fun invoke(email: String): FormInputState {
        val isEmailValid = emailValidator.invoke(email)
        if (isEmailValid) {
            signInFormPersistence.setEmail(email)
        }

        val errorMessage: String? = if (isEmailValid) {
            null
        } else {
            stringResourceProvider.getString(R.string.sign_in_form_email_error)
        }


        return FormInputState(isValid = isEmailValid, errorMessage = errorMessage)
    }
}
