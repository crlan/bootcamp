package com.apprendev.bootcamp.domain

import androidx.annotation.StringRes

interface AndroidStringResourceProvider {
    fun getString(@StringRes identifier: Int): String
}
