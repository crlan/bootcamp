package com.apprendev.bootcamp.entities

data class FormInputState(
    val isValid: Boolean,
    val errorMessage: String? = null
)
