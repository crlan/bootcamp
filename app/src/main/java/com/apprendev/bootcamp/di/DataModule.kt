package com.apprendev.bootcamp.di

import android.content.Context
import androidx.room.Room
import com.apprendev.bootcamp.data.database.BootCampDatabase
import com.apprendev.bootcamp.data.database.MoviesDao
import com.apprendev.bootcamp.data.database.UserDao
import com.apprendev.bootcamp.data.popularmovies.TheMovieDbServices
import com.apprendev.bootcamp.domain.*
import com.apprendev.bootcamp.presentation.fragments.signup.SignUpFormPersistence
import com.apprendev.bootcamp.presentation.fragments.signup.SignUpFormPersistenceImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class DataModule {

    @Singleton
    @Provides
    fun getDatabase(@ApplicationContext context: Context): BootCampDatabase {
        return Room.databaseBuilder(
            context,
            BootCampDatabase::class.java,
            BootCampDatabase::class.java.name
        ).fallbackToDestructiveMigration()
            .build()
    }

    @Singleton
    @Provides
    fun getUsersDao(database: BootCampDatabase): UserDao {
        return database.userDao()
    }

    @Singleton
    @Provides
    fun getMoviesDao(database: BootCampDatabase): MoviesDao {
        return database.moviesDao()
    }

    @Singleton
    @Provides
    fun getApiConfiguration(apiServices: TheMovieDbServices): ApiConfigurationApi {
        return CacheApiConfigurationApi(apiServices)
    }

    @Provides
    fun getSignUpFormPersistence(): SignUpFormPersistence {
        return SignUpFormPersistenceImpl()
    }
}


