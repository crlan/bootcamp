package com.apprendev.bootcamp.di

import android.content.Context
import com.apprendev.bootcamp.data.AndroidStringResourceProviderImpl
import com.apprendev.bootcamp.data.database.UserDao
import com.apprendev.bootcamp.domain.*
import com.apprendev.bootcamp.presentation.fragments.signup.SignUpFormPersistence
import com.apprendev.bootcamp.presentation.fragments.signup.SignUpFormValidatorUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@InstallIn(SingletonComponent::class)
@Module
class DomainModule {
    @Provides
    fun getNameValidator(): NameValidator {
        return NameValidator()
    }

    @Provides
    fun getEmailValidator(): EmailValidator {
        return EmailValidator()
    }

    @Provides
    fun getPasswordValidator(): PasswordValidator {
        return PasswordValidator()
    }

    @Provides
    fun getSignUpFormValidatorUseCase(
        userDao: UserDao,
        signUpFormPersistence: SignUpFormPersistence
    ): SignUpFormValidatorUseCase {
        return SignUpFormValidatorUseCase(signUpFormPersistence, userDao)
    }

    @Provides
    fun getSignInFormPersistence(): SignInFormPersistence {
        return SignInFormPersistenceImpl()
    }

    @Provides
    fun getEmailHasBeenChangedUseCase(
        emailValidator: EmailValidator,
        signInFormPersistence: SignInFormPersistence,
        androidStringResourceProvider: AndroidStringResourceProvider
    ): EmailHasBeenChangedUseCase {
        return EmailHasBeenChangedUseCase(
            emailValidator = emailValidator,
            signInFormPersistence = signInFormPersistence,
            stringResourceProvider = androidStringResourceProvider
        )
    }

    @Provides
    fun getPasswordHasBeenChangedUseCase(
        passwordValidator: PasswordValidator,
        signInFormPersistence: SignInFormPersistence,
        androidStringResourceProvider: AndroidStringResourceProvider
    ): PasswordHasBeenChangedUseCase {
        return PasswordHasBeenChangedUseCase(
            passwordValidator = passwordValidator,
            signInFormPersistence = signInFormPersistence,
            stringResourceProvider = androidStringResourceProvider
        )
    }

    @Provides
    fun getAndroidStringResourceProvider(@ApplicationContext context: Context): AndroidStringResourceProvider {
        return AndroidStringResourceProviderImpl(context)
    }
}
