package com.apprendev.bootcamp.di

import com.apprendev.bootcamp.data.cart.CartResponse
import com.apprendev.bootcamp.data.popularmovies.TheMovieDbServices
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class NetworkModule {

    @Singleton
    @Provides
    fun getNetworkClient(): OkHttpClient {
        val logging = HttpLoggingInterceptor()
        logging.setLevel(HttpLoggingInterceptor.Level.BODY)
        return OkHttpClient.Builder()
            .addInterceptor(logging)
            .build()
    }


    @Singleton
    @Provides
    fun getNetworkWrapper(networkClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .baseUrl("https://api.themoviedb.org/3/")
            .client(networkClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    @Singleton
    @Provides
    fun getApiServices(networkWrapper: Retrofit): TheMovieDbServices {
        return networkWrapper.create(TheMovieDbServices::class.java)
    }


    @Singleton
    @Provides
    fun getLocalServices(networkClient: OkHttpClient): LocalServices {
        val retrofit = Retrofit.Builder()
            .baseUrl("http://192.168.100.37:8080")
            .client(networkClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        return retrofit.create(LocalServices::class.java)
    }
}


interface LocalServices {
    @GET("/cart")
    suspend fun getCart(): CartResponse
}
