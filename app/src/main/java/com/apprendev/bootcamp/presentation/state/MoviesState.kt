package com.apprendev.bootcamp.presentation.state

import com.apprendev.bootcamp.data.database.entities.MovieEntity
import com.apprendev.bootcamp.data.details.MovieDetails
import com.apprendev.bootcamp.data.popularmovies.Movie

sealed class MoviesState {
    object Loading : MoviesState()
    class MoviesReady(val movieList: List<MovieEntity>) : MoviesState()
    class MovieDetailsReady(val movieDetails: MovieDetails) : MoviesState()
    class RecentMovieReady(val movie: MovieDetails) : MoviesState()
    class Errors(error: Throwable) : MoviesState()
}
