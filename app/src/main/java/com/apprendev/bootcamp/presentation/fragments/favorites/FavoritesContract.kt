package com.apprendev.bootcamp.presentation.fragments.favorites

import com.apprendev.bootcamp.data.database.entities.MovieEntity

interface FavoritesContract {

    fun addFavoriteMovie(movie: MovieEntity)

    fun deleteFavoriteMovie(movie: MovieEntity)
}
