package com.apprendev.bootcamp.presentation.fragments.signIn

data class SignInForm(
    var email: String? = null,
    var password: String? = null
)
