package com.apprendev.bootcamp.presentation.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import coil.load
import com.apprendev.bootcamp.R
import com.apprendev.bootcamp.data.details.MovieDetails
import com.apprendev.bootcamp.databinding.MovieDetailsFragmentBinding
import com.apprendev.bootcamp.presentation.state.MoviesState
import com.apprendev.bootcamp.presentation.viewmodels.MoviesViewModel
import dagger.hilt.android.AndroidEntryPoint

const val MOVIE_ID = "MOVIE_ID"

@AndroidEntryPoint
class MovieDetailsFragment : Fragment() {
    private lateinit var binding: MovieDetailsFragmentBinding
    private val viewModel: MoviesViewModel by viewModels()
    private var movieId: String = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = with(MovieDetailsFragmentBinding.inflate(inflater, container, false)) {
        binding = this
        binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        arguments?.let { bundle ->
            movieId = bundle[MOVIE_ID] as String
        }

        viewModel.state.observe(this, Observer { movieState ->
            when (movieState) {
                is MoviesState.Loading -> showLoading()
                is MoviesState.MovieDetailsReady -> displayMovieDetails(movieState.movieDetails)
                is MoviesState.Errors -> TODO()
                is MoviesState.RecentMovieReady -> displayMovieDetails(movieState.movie)
                else -> {
                }
            }
        })

        viewModel.getMovieDetails(movieId)
    }

    private fun displayMovieDetails(movieDetails: MovieDetails) {
        binding.movieName.text = movieDetails.original_title
        binding.movieOverview.text = movieDetails.overview

        binding.movieImage.load(movieDetails.backdrop_path) {
            error(R.drawable.ic_image_broke)
        }

        binding.productionCountriesValue.text = movieDetails
            .production_companies
            ?.map { it.name }
            .toString()
    }


    private fun showLoading() {
    }

}
