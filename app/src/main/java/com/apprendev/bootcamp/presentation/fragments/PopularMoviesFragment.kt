package com.apprendev.bootcamp.presentation.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import com.apprendev.bootcamp.databinding.FragmentMoviesListBinding
import com.apprendev.bootcamp.presentation.BaseMovieHolderAbstractFactory
import com.apprendev.bootcamp.presentation.MovieHolderFactory
import com.apprendev.bootcamp.presentation.MoviesListAdapter
import com.apprendev.bootcamp.presentation.state.MoviesState
import com.apprendev.bootcamp.presentation.viewmodels.MoviesViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@AndroidEntryPoint
class PopularMoviesFragment : Fragment() {
    lateinit var binding: FragmentMoviesListBinding
    private val viewModel: MoviesViewModel by viewModels()
    private var searchJob: Job? = null
    private val adapter: MoviesListAdapter by lazy {
        MoviesListAdapter(factory = getBaseMovieHolderFactory())
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = with(FragmentMoviesListBinding.inflate(inflater, container, false)) {
        binding = this
        binding.recyclerView.adapter = adapter
        this.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.state.observe(this, Observer { movieState ->
            when (movieState) {
                is MoviesState.Loading ->  {}
                is MoviesState.MoviesReady -> {
                }
                is MoviesState.Errors -> TODO()
                is MoviesState.MovieDetailsReady -> {
                }
                is MoviesState.RecentMovieReady -> TODO()
            }
        })

        fetchData()
    }

    private fun fetchData() {
        // Make sure we cancel the previous job before creating a new one
        searchJob?.cancel()
        searchJob = lifecycleScope.launch {
            viewModel.fetchPopularMovies().collectLatest {
                binding.recyclerView.visibility = View.VISIBLE
                binding.progressBar.visibility = View.GONE
                adapter.submitData(it)
            }
        }
    }

    private fun getBaseMovieHolderFactory(): BaseMovieHolderAbstractFactory =
        MovieHolderFactory(true, viewModel)
}
