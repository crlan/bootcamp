package com.apprendev.bootcamp.presentation

import androidx.recyclerview.widget.DiffUtil
import com.apprendev.bootcamp.data.database.entities.MovieEntity

object MoviesDiffCallback : DiffUtil.ItemCallback<MovieEntity>() {
    override fun areItemsTheSame(oldItem: MovieEntity, newItem: MovieEntity): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: MovieEntity, newItem: MovieEntity): Boolean {
        return oldItem == newItem
    }
}
