package com.apprendev.bootcamp.presentation

import android.os.Bundle
import androidx.navigation.findNavController
import coil.load
import com.apprendev.bootcamp.R
import com.apprendev.bootcamp.data.database.entities.MovieEntity
import com.apprendev.bootcamp.data.popularmovies.Movie
import com.apprendev.bootcamp.databinding.GridMovieHolderBinding
import com.apprendev.bootcamp.presentation.fragments.MOVIE_ID

class GridMovieHolder(binding: GridMovieHolderBinding) :
    BaseMovieHolder<GridMovieHolderBinding>(binding) {

    override fun bind(movie: MovieEntity) {
        with(binding) {
            movieName.text = movie.title
            movieImage.load(movie.backdrop_path)
            releaseDate.text = ""
            overview.text = movie.overview
            popularity.text = "Popularity: ${movie.popularity}"

            seeMovieDetails.setOnClickListener {
                it.findNavController()
                    .navigate(
                        R.id.action_popularMovies_to_movieDetails,
                        Bundle().apply {
                            putString(MOVIE_ID, movie.id)
                        }
                    )
            }
        }
    }
}
