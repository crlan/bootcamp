package com.apprendev.bootcamp.presentation.fragments.signup

sealed class SignUpFormState() {
    object SignUpFormIdle : SignUpFormState()
    object SignUpFormComplete : SignUpFormState()
    object SignUpFormIncomplete : SignUpFormState()
    class SignUpFormError(val error: Exception) : SignUpFormState()
    object SignUpFormSubmitted : SignUpFormState()
}
