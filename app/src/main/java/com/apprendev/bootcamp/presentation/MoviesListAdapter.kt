package com.apprendev.bootcamp.presentation

import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import com.apprendev.bootcamp.data.database.entities.MovieEntity

class MoviesListAdapter(
    private val factory: BaseMovieHolderAbstractFactory
) : PagingDataAdapter<MovieEntity, BaseMovieHolder<*>>(MoviesDiffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseMovieHolder<*> =
        factory.buildBaseMovieHolder(parent)

    override fun onBindViewHolder(holderLinear: BaseMovieHolder<*>, position: Int) {
        val item = getItem(position)
        if (item != null) {
            holderLinear.bind(item)
        }

    }
}
