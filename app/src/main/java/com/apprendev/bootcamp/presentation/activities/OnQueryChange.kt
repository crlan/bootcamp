package com.apprendev.bootcamp.presentation.activities

import androidx.appcompat.widget.SearchView

class OnQueryChange(private val callback: QueryHasBeenChanged) : SearchView.OnQueryTextListener {

    override fun onQueryTextSubmit(query: String?): Boolean {
        query?.let {
            callback.invoke(it)
        }
        return false
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        return true
    }

}
