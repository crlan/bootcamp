package com.apprendev.bootcamp.presentation.fragments.signIn

import com.apprendev.bootcamp.entities.FormInputState

sealed class SignInUIState {
    object SignInFormReady : SignInUIState()
    object SignInFormIdle : SignInUIState()

    class SignInFormUpdated(
        val emailFormState: FormInputState?,
        val passwordFormState: FormInputState?
    ) : SignInUIState()
}
