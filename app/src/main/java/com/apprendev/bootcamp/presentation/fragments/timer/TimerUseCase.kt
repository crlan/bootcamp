package com.apprendev.bootcamp.presentation.fragments.timer

import io.reactivex.rxjava3.core.Observable
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class TimerUseCase @Inject constructor() {

    operator fun invoke(seconds: Long): Observable<String> =
        Observable.intervalRange(
            0,
            seconds,
            1,
            1,
            TimeUnit.SECONDS
        )
            .map {
                (it + 1).toString()
            }
}
