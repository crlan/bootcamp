package com.apprendev.bootcamp.presentation.menu

import androidx.annotation.DrawableRes

data class MenuData(
    val label: String,
    @DrawableRes val icon: Int,
    val colorAsString: String? = null,
    val animationUrl: String
)
