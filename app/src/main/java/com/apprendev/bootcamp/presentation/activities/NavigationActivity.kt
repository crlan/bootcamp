package com.apprendev.bootcamp.presentation.activities

import android.R.attr
import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.os.bundleOf
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.setupWithNavController
import com.apprendev.bootcamp.R
import com.apprendev.bootcamp.data.ConfigurationProvider
import com.apprendev.bootcamp.data.FirebaseRemoteConfigConfigurationProvider
import com.apprendev.bootcamp.data.remoteconfig.FirebaseMenu
import com.apprendev.bootcamp.databinding.ActivityNavigationBinding
import com.apprendev.bootcamp.presentation.fragments.search.QUERY_KEY
import com.apprendev.bootcamp.presentation.menu.MenuAdapter
import com.apprendev.bootcamp.presentation.menu.MenuData
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.ktx.remoteConfig
import com.google.firebase.remoteconfig.ktx.remoteConfigSettings
import com.google.zxing.integration.android.IntentIntegrator
import dagger.hilt.android.AndroidEntryPoint


internal typealias QueryHasBeenChanged = (query: String) -> Unit
internal typealias MenuClicked = () -> Unit

const val TAG = "NavigationActivity"

@AndroidEntryPoint
class NavigationActivity : AppCompatActivity(), ConfigurationProvider.ConfigurationHasBeenFetched {

    private lateinit var binding: ActivityNavigationBinding
    private lateinit var navController: NavController
    private lateinit var remoteConfig: FirebaseRemoteConfig

    private lateinit var configurationProvider: ConfigurationProvider


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityNavigationBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)
        handleIntent(intent)


        remoteConfig = Firebase.remoteConfig
        val configSettings = remoteConfigSettings {
            minimumFetchIntervalInSeconds = 5
        }

        remoteConfig.setConfigSettingsAsync(configSettings)

        configurationProvider = FirebaseRemoteConfigConfigurationProvider(this, this)

        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.w(TAG, "Fetching FCM registration token failed", task.exception)
                return@OnCompleteListener
            }

            // Get new FCM registration token
            val token = task.result

            // Log and toast
            val msg = getString(R.string.msg_token_fmt) + "  $token"
            Log.d(TAG, msg)
            Toast.makeText(baseContext, msg, Toast.LENGTH_SHORT).show()
        })
    }

    override fun onResume() {
        super.onResume()

        navController = findNavController(R.id.nav_host_fragment)
        NavigationUI.setupWithNavController(binding.bottomNavigation, navController)
        val appBarConfiguration = AppBarConfiguration(navController.graph, binding.drawerLayout)
        binding.toolbar.setupWithNavController(navController, appBarConfiguration)

    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.toolbar_menu, menu)

        // Associate searchable configuration with the SearchView
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager

        (menu.findItem(R.id.search).actionView as SearchView)
            .apply {
                setSearchableInfo(searchManager.getSearchableInfo(componentName))
                setOnQueryTextListener(OnQueryChange(::queryHasBeenChanged))
            }


        return true
    }

    private fun queryHasBeenChanged(query: String) {
        binding.toolbar.collapseActionView()

        navController.navigate(
            R.id.action_global_searchFor,
            bundleOf(Pair(QUERY_KEY, query))
        )
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        intent?.let {
            handleIntent(it)
        }
    }

    private fun handleIntent(intent: Intent) {
        if (Intent.ACTION_SEARCH == intent.action) {
            val query = intent.getStringExtra(SearchManager.QUERY)
            //use the query to search your data somehow
            println("----> $query")
        }
    }

    override fun completed() {
        val remoteConfigMenu =
            configurationProvider.getConfigurationValue("drawer_menu", FirebaseMenu::class.java)

        val menuData: List<MenuData> = remoteConfigMenu
            .menu
            .map { menu ->
                MenuData(
                    label = menu.menuTitle,
                    icon = R.drawable.ic_right_arrow,
                    colorAsString = menu.menuColor,
                    animationUrl = menu.animation
                )
            }

        binding.menuList.adapter = MenuAdapter(navController, ::dismissDrawer)
        (binding.menuList.adapter as? MenuAdapter)?.submitList(menuData)
    }

    private fun dismissDrawer() {
        binding.drawerLayout.closeDrawers()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        if(result != null) {
            if(result.contents == null) {
                Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, "Scanned: " + result.contents, Toast.LENGTH_LONG).show()
                println("Codigo = ${result.contents}")
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}


