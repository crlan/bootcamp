package com.apprendev.bootcamp.presentation.fragments.favorites

import androidx.lifecycle.LiveData
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.apprendev.bootcamp.data.database.MoviesDao
import com.apprendev.bootcamp.data.database.entities.MovieEntity
import com.apprendev.bootcamp.domain.FAVORITES_MOVIES_LIST
import com.apprendev.bootcamp.presentation.viewmodels.AddFavoriteMovieUseCase
import com.apprendev.bootcamp.presentation.viewmodels.BaseListViewModel
import com.apprendev.bootcamp.presentation.viewmodels.RemoveFavoriteMovieUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class FavoritesViewModel @Inject constructor(
    private val moviesDao: MoviesDao,
    private val addFavoriteMovieUseCase: AddFavoriteMovieUseCase,
    private val removeFavoriteMovieUseCase: RemoveFavoriteMovieUseCase,
) : BaseListViewModel(), FavoritesContract {

    val favoriteMovies: LiveData<List<MovieEntity>> = moviesDao
        .getMoviesByListName(
            FAVORITES_MOVIES_LIST
        ).asLiveData()

    override fun addFavoriteMovie(movie: MovieEntity) {
        viewModelScope.launch(Dispatchers.IO) {
            addFavoriteMovieUseCase.invoke(movie)
        }
    }

    override fun deleteFavoriteMovie(movie: MovieEntity) {
        viewModelScope.launch(Dispatchers.IO) {
            removeFavoriteMovieUseCase.invoke(movie)
        }
    }
}
