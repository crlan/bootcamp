package com.apprendev.bootcamp.presentation.fragments.signup

interface SignUpFormPersistence {
    fun getSignUpForm(): SignUpForm
    fun updateSignUpForm(form: SignUpForm)
}

