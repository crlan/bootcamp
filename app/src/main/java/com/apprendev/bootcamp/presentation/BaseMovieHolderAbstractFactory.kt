package com.apprendev.bootcamp.presentation

import android.view.ViewGroup

interface BaseMovieHolderAbstractFactory {
    fun buildBaseMovieHolder(parent: ViewGroup): BaseMovieHolder<*>
}
