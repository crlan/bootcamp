package com.apprendev.bootcamp.presentation.fragments.favorites

import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import com.apprendev.bootcamp.data.database.entities.MovieEntity
import com.apprendev.bootcamp.presentation.BaseMovieHolder
import com.apprendev.bootcamp.presentation.BaseMovieHolderAbstractFactory
import com.apprendev.bootcamp.presentation.MoviesDiffCallback

class FavoritesMoviesAdapter(private val factory: BaseMovieHolderAbstractFactory) :
    ListAdapter<MovieEntity, BaseMovieHolder<*>>(MoviesDiffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseMovieHolder<*> {
        return factory.buildBaseMovieHolder(parent)
    }

    override fun onBindViewHolder(holder: BaseMovieHolder<*>, position: Int) {
        holder.bind(getItem(position))
    }
}
