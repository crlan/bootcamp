package com.apprendev.bootcamp.presentation.fragments.timer

sealed class TimerState {
    object TimerIdle : TimerState()
    class TimerUpdate(val timer: String) : TimerState()
}
