package com.apprendev.bootcamp.presentation.fragments.timer

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.apprendev.bootcamp.presentation.fragments.timer.TimerState
import com.apprendev.bootcamp.presentation.fragments.timer.TimerUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject

@HiltViewModel
class TimerViewModel @Inject constructor(
    private val timerUseCase: TimerUseCase
) : ViewModel() {

    val timerState: MutableLiveData<TimerState> = MutableLiveData(TimerState.TimerIdle)

    private val disposable = CompositeDisposable()

    fun initTimer(seconds: Long) {
        disposable
            .add(
                timerUseCase
                    .invoke(seconds)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ timer ->
                        println("Thread -> ${Thread.currentThread().name}")
                        timerState.postValue(TimerState.TimerUpdate(timer))
                    }, {
                        it.printStackTrace()
                    })
            )
    }


    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }
}


