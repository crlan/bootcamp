package com.apprendev.bootcamp.presentation

import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.apprendev.bootcamp.data.database.entities.MovieEntity
import com.apprendev.bootcamp.data.popularmovies.Movie

abstract class BaseMovieHolder<T : ViewDataBinding>(val binding: T) :
    RecyclerView.ViewHolder(binding.root) {

    abstract fun bind(movie: MovieEntity)
}
