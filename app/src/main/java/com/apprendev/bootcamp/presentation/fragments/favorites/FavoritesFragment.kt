package com.apprendev.bootcamp.presentation.fragments.favorites

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.apprendev.bootcamp.data.database.entities.MovieEntity
import com.apprendev.bootcamp.databinding.FragmentMoviesListBinding
import com.apprendev.bootcamp.presentation.*
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class FavoritesFragment : Fragment() {
    private val viewModel: FavoritesViewModel by viewModels()
    lateinit var binding: FragmentMoviesListBinding
    private val adapter: FavoritesMoviesAdapter by lazy {
        FavoritesMoviesAdapter(getBaseMovieHolderFactory())
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = with(FragmentMoviesListBinding.inflate(inflater, container, false)) {
        binding = this
        this.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.recyclerView.adapter = adapter

        viewModel.favoriteMovies.observe(this, {
            setUpMovies(it)
        })
    }

    private fun setUpMovies(popularMovies: List<MovieEntity>) {
        adapter.submitList(popularMovies)
        binding.recyclerView.visibility = View.VISIBLE
        binding.progressBar.visibility = View.GONE
    }

    private fun getBaseMovieHolderFactory(): BaseMovieHolderAbstractFactory =
        MovieHolderFactory(true, viewModel)
}

