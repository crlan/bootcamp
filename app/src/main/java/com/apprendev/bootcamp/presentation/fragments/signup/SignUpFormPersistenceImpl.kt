package com.apprendev.bootcamp.presentation.fragments.signup

class SignUpFormPersistenceImpl : SignUpFormPersistence {
    private var signUpForm: SignUpForm = SignUpForm()

    override fun getSignUpForm(): SignUpForm = signUpForm

    override fun updateSignUpForm(form: SignUpForm) {
        signUpForm = form
    }
}
