package com.apprendev.bootcamp.presentation.viewmodels

import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.apprendev.bootcamp.data.database.entities.MovieEntity
import com.apprendev.bootcamp.domain.GetMovieDetailsUseCase
import com.apprendev.bootcamp.domain.GetRecentMovieUseCase
import com.apprendev.bootcamp.domain.LocalCartRepository
import com.apprendev.bootcamp.domain.PopularMoviesRepository
import com.apprendev.bootcamp.presentation.fragments.favorites.FavoritesContract
import com.apprendev.bootcamp.presentation.state.MoviesState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MoviesViewModel @Inject constructor(
    private val movieDetailsUseCase: GetMovieDetailsUseCase,
    private val getRecentMovieUseCase: GetRecentMovieUseCase,
    private val addFavoriteMovieUseCase: AddFavoriteMovieUseCase,
    private val removeFavoriteMovieUseCase: RemoveFavoriteMovieUseCase,
    private val repository: PopularMoviesRepository,
    private val localCartRepository: LocalCartRepository
) : BaseListViewModel(), FavoritesContract {
    private var currentSearchResult: Flow<PagingData<MovieEntity>>? = null


    init {
        state.value = MoviesState.Loading
        fetchLocalCart()
    }

    private fun fetchLocalCart() {
        viewModelScope.launch {
//            println("--> ${localCartRepository.invoke()}")
        }
    }

    suspend fun fetchPopularMovies(): Flow<PagingData<MovieEntity>> {
        val newResult: Flow<PagingData<MovieEntity>> =
            repository.getPopularMoviesResultStream().cachedIn(viewModelScope)
        currentSearchResult = newResult
        return newResult
    }

    fun getMovieDetails(movieId: String) {
        if (movieId.isEmpty()) {
            return getRecentMovie()
        }
        viewModelScope.launch(Dispatchers.IO) {
            state.postValue(MoviesState.MovieDetailsReady(movieDetailsUseCase.invoke(movieId)))
        }
    }

    private fun getRecentMovie() {
        viewModelScope.launch(Dispatchers.IO) {
            val movie = getRecentMovieUseCase.invoke()
            state.postValue(MoviesState.RecentMovieReady(movie))
        }
    }

    override fun addFavoriteMovie(movie: MovieEntity) {
        viewModelScope.launch(Dispatchers.IO) {
            addFavoriteMovieUseCase.invoke(movie)
        }
    }

    override fun deleteFavoriteMovie(movie: MovieEntity) {
        viewModelScope.launch(Dispatchers.IO) {
            removeFavoriteMovieUseCase.invoke(movie)
        }
    }
}


