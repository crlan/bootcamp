package com.apprendev.bootcamp.presentation.menu

import android.graphics.Color
import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.apprendev.bootcamp.R
import com.apprendev.bootcamp.databinding.MenuItemHolderBinding
import com.apprendev.bootcamp.presentation.activities.MenuClicked

class MenuHolder(
    private val binding: MenuItemHolderBinding,
    private val navController: NavController,
    private val callback: MenuClicked
) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(data: MenuData) {
        binding.menuData = data
        binding.container.setCardBackgroundColor(Color.parseColor(data.colorAsString))
        binding.container.setOnClickListener {
            callback.invoke()
            navController
                .navigate(R.id.action_global_animationFragment, Bundle().apply {
                    putString("animation", data.animationUrl)
                })
        }
        binding.executePendingBindings()
    }
}
