package com.apprendev.bootcamp.presentation.fragments.signup

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.apprendev.bootcamp.R
import com.apprendev.bootcamp.databinding.FragmentSignUpBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SignUpFragment : Fragment() {
    private lateinit var binding: FragmentSignUpBinding

    private val viewModel: SignUpViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = with(FragmentSignUpBinding.inflate(inflater, container, false)) {
        binding = this
        binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.state.observe(this, Observer {
            when (it) {
                SignUpFormState.SignUpFormComplete -> {
                    binding.signUp.isEnabled = true
                }
                SignUpFormState.SignUpFormIdle -> {
                }
                SignUpFormState.SignUpFormIncomplete -> {
                    binding.signUp.isEnabled = false
                }
                is SignUpFormState.SignUpFormError -> {
                    manageError(it.error)
                }
                SignUpFormState.SignUpFormSubmitted -> {
                    manageFormSubmitted()
                }
            }
        })

        binding.email.addTextChangedListener {
            binding.textInputLayoutEmail.error = null
            viewModel.emailHasBeenChanged(it.toString())
        }
        binding.password.addTextChangedListener { viewModel.passwordHasBeenChanged(it.toString()) }
        binding.name.addTextChangedListener { viewModel.nameHasBeenChanged(it.toString()) }
        binding.lastName.addTextChangedListener { viewModel.lastNameHasBeenChanged(it.toString()) }

        binding.signUp.setOnClickListener { viewModel.signUp() }
    }

    private fun manageFormSubmitted() {
        binding.textInputLayoutName.visibility = View.GONE
        binding.textInputLayoutLastName.visibility = View.GONE
        binding.textInputLayoutEmail.visibility = View.GONE
        binding.textInputLayoutPassword.visibility = View.GONE
        binding.signUp.visibility = View.GONE

        binding.animationView.visibility = View.VISIBLE

        binding.animationView.setOnClickListener {
            findNavController().navigate(R.id.action_signUp_to_popularMovies)
        }
    }

    private fun manageError(error: Exception) {
        when (error) {
            is DuplicatedEmailException -> {
                binding.textInputLayoutEmail.error = error.message
            }
            else -> {
                error.printStackTrace()
            }
        }
    }
}

