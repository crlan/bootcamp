package com.apprendev.bootcamp.presentation.viewmodels

import com.apprendev.bootcamp.data.database.MoviesDao
import com.apprendev.bootcamp.data.database.entities.MovieEntity
import com.apprendev.bootcamp.domain.FAVORITES_MOVIES_LIST
import javax.inject.Inject
import kotlin.random.Random

class AddFavoriteMovieUseCase @Inject constructor(private val moviesDao: MoviesDao) {
    suspend fun invoke(movie: MovieEntity) {

        moviesDao.updateUserMovie(
            movie.copy(
                isFavorite = true
            )
        )

        moviesDao.insertUserMovie(
            movie.copy(
                listName = FAVORITES_MOVIES_LIST,
                movieId = Random.nextLong(),
                isFavorite = true
            )
        )
    }
}

