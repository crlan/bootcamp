package com.apprendev.bootcamp.presentation.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.apprendev.bootcamp.presentation.state.MoviesState
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job

abstract class BaseListViewModel(
    val state: MutableLiveData<MoviesState> = MutableLiveData<MoviesState>(),
    val scope: CoroutineScope = CoroutineScope(Job() + Dispatchers.Main)
) : ViewModel()
