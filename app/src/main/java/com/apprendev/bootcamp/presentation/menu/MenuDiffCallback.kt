package com.apprendev.bootcamp.presentation.menu

import androidx.recyclerview.widget.DiffUtil

object MenuDiffCallback : DiffUtil.ItemCallback<MenuData>() {

    override fun areItemsTheSame(oldItem: MenuData, newItem: MenuData): Boolean = oldItem == newItem

    override fun areContentsTheSame(oldItem: MenuData, newItem: MenuData): Boolean =
        oldItem == newItem

}
