package com.apprendev.bootcamp.presentation.fragments.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import com.apprendev.bootcamp.data.database.entities.MovieEntity
import com.apprendev.bootcamp.databinding.FragmentMoviesListBinding
import com.apprendev.bootcamp.presentation.BaseMovieHolderAbstractFactory
import com.apprendev.bootcamp.presentation.MovieHolderFactory
import com.apprendev.bootcamp.presentation.MoviesListAdapter
import com.apprendev.bootcamp.presentation.state.MoviesState
import dagger.hilt.android.AndroidEntryPoint


internal const val QUERY_KEY = "QUERY_KEY"

@AndroidEntryPoint
class SearchMoviesFragment : Fragment() {
    private val args: SearchMoviesFragmentArgs by navArgs()
    private val viewModel: SearchViewModel by viewModels()
    private lateinit var binding: FragmentMoviesListBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = with(FragmentMoviesListBinding.inflate(inflater, container, false)) {
        binding = this
        this.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.state.observe(this, Observer { movieState ->
            when (movieState) {
                is MoviesState.Loading -> applyLoadingState()
                is MoviesState.MoviesReady -> showReadyState(movieState.movieList)
                is MoviesState.Errors -> TODO()
                is MoviesState.MovieDetailsReady -> {
                }
                is MoviesState.RecentMovieReady -> TODO()
            }
        })

        fetchData()
    }

    private fun showReadyState(movies: List<MovieEntity>) {
        if (binding.recyclerView.adapter == null) {
            binding.recyclerView.adapter = MoviesListAdapter(factory = getBaseMovieHolderFactory())
        }

//        (binding.recyclerView.adapter as? MoviesListAdapter)?.submitList(movies)
//        binding.recyclerView.visibility = View.VISIBLE
//        binding.progressBar.visibility = View.GONE

    }

    private fun applyLoadingState() {
        binding.recyclerView.visibility = View.GONE
        binding.progressBar.visibility = View.VISIBLE
    }

    private fun fetchData() {
        viewModel.searchFor(args.QUERYKEY)
    }

    private fun getBaseMovieHolderFactory(): BaseMovieHolderAbstractFactory =
        MovieHolderFactory(false, viewModel)

}

