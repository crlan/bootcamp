package com.apprendev.bootcamp.presentation.menu

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.NavController
import androidx.recyclerview.widget.ListAdapter
import com.apprendev.bootcamp.databinding.MenuItemHolderBinding
import com.apprendev.bootcamp.presentation.activities.MenuClicked


class MenuAdapter(private val navController: NavController, private val callback: MenuClicked) :
    ListAdapter<MenuData, MenuHolder>(MenuDiffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuHolder = MenuHolder(
        binding = MenuItemHolderBinding.inflate(LayoutInflater.from(parent.context), parent, false),
        navController = navController,
        callback = callback
    )

    override fun onBindViewHolder(holder: MenuHolder, position: Int) {
        holder.bind(getItem(position))
    }

}


