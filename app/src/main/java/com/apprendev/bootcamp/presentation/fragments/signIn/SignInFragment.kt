package com.apprendev.bootcamp.presentation.fragments.signIn

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.apprendev.bootcamp.R
import com.apprendev.bootcamp.databinding.SignInFragmentBinding
import com.apprendev.bootcamp.presentation.activities.NavigationActivity
import com.google.android.material.snackbar.Snackbar
import com.google.zxing.integration.android.IntentIntegrator
import dagger.hilt.android.AndroidEntryPoint

const val TAG: String = "SignInFragment"

@AndroidEntryPoint
class SignInFragment : Fragment() {
    lateinit var binding: SignInFragmentBinding
    private val viewModel: SignInViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = with(SignInFragmentBinding.inflate(inflater, container, false)) {
        binding = this
        binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding
            .email
            .addTextChangedListener {
                binding.textInputLayoutEmail.error = null
                viewModel.emailHasBeenChanged(it.toString())
            }

        binding
            .pasword
            .addTextChangedListener {
                binding.textInputLayoutPassword.error = null
                viewModel.passwordHasBeenChanged(it.toString())
            }

        binding
            .signIn
            .setOnClickListener {
                showActivity()
            }


        viewModel.signInState.observe(this, Observer { signInState ->
            when (signInState) {
                is SignInUIState.SignInFormIdle -> {
                }
                is SignInUIState.SignInFormReady -> TODO()

                is SignInUIState.SignInFormUpdated -> updateFormUI(signInState)
            }

        })
    }

    private fun showActivity() {
        val intent = IntentIntegrator(requireActivity())
        with(intent){
            this.setPrompt(getString(R.string.barcode_promt_label))
            this.setBeepEnabled(true)
            this.setBarcodeImageEnabled(true)
            this.initiateScan()
        }
    }

    private fun updateFormUI(signInFormUIState: SignInUIState.SignInFormUpdated) {
        signInFormUIState.emailFormState?.let { emailFormState ->
            binding.textInputLayoutEmail.error = emailFormState.errorMessage
        }

        signInFormUIState.passwordFormState?.let { passwordFormState ->
            binding.textInputLayoutPassword.error = passwordFormState.errorMessage
        }
    }
}


fun AppCompatActivity.showMessage(text: String, view: View) {
    Snackbar.make(view, text, Snackbar.LENGTH_SHORT).show()
}


fun Fragment.showMessage(text: String, view: View) {
    Snackbar.make(view, text, Snackbar.LENGTH_SHORT).show()
}

