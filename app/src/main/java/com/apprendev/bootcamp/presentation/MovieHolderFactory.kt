package com.apprendev.bootcamp.presentation

import android.view.LayoutInflater
import android.view.ViewGroup
import com.apprendev.bootcamp.databinding.GridMovieHolderBinding
import com.apprendev.bootcamp.databinding.LinearMovieHolderBinding
import com.apprendev.bootcamp.presentation.viewmodels.BaseListViewModel
import com.apprendev.bootcamp.presentation.viewmodels.MoviesViewModel

class MovieHolderFactory(
    private val buildLinearHolder: Boolean = true,
    private val viewModel: BaseListViewModel? = null
) :
    BaseMovieHolderAbstractFactory {

    override fun buildBaseMovieHolder(parent: ViewGroup): BaseMovieHolder<*> =
        if (buildLinearHolder) {
            LinearMovieHolder(
                binding = LinearMovieHolderBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                ),
                viewModel = viewModel
            )
        } else {
            GridMovieHolder(
                GridMovieHolderBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
        }

}
