package com.apprendev.bootcamp.presentation.fragments.signup

import com.apprendev.bootcamp.data.database.UserDao
import javax.inject.Inject

class SignUpFormValidatorUseCase @Inject constructor(
    private val signUpFormApi: SignUpFormPersistence,
    private val usersDao: UserDao
) {
    suspend fun invoke(): Boolean {
        val form = signUpFormApi.getSignUpForm()
        // form validation code
        val isFormComplete =
            form.email != null && form.password != null && form.name != null && form.lastName != null

        if (isFormComplete) {
            // validar que el email no este guardado
            val userEmail = form.email
            userEmail?.let { email ->
                val user = usersDao.findByEmail(email)
                if (user != null) {// el usuario ya fue registrado
                    throw DuplicatedEmailException()
                }
            }
        }

        return isFormComplete
    }
}


class DuplicatedEmailException() : Exception("El email ya fue registrado previamente")

