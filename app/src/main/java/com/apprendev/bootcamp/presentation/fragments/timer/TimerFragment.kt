package com.apprendev.bootcamp.presentation.fragments.timer

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.apprendev.bootcamp.databinding.TimerFragmentBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class TimerFragment : Fragment() {

    private val viewModel: TimerViewModel by viewModels()
    private lateinit var binding: TimerFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = with(TimerFragmentBinding.inflate(inflater, container, false)) {
        binding = this
        binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.initTimer.setOnClickListener {
            viewModel.initTimer(binding.seconds.text.toString().toLong())
        }

        binding.plusSeconds.setOnClickListener {
            plusSeconds()
        }

        binding.minusSeconds.setOnClickListener {
            minusSeconds()
        }

        viewModel.timerState.observe(this, Observer { state ->
            when (state) {
                is TimerState.TimerIdle -> {
                    println("Idle state")
                }
                is TimerState.TimerUpdate -> {
                    binding.timer.text = state.timer
                }
            }

        })
    }

    private fun plusSeconds() {
        val currentValue = binding.seconds.text.toString().toInt()
        binding.seconds.text = (currentValue + 1).toString()
    }

    private fun minusSeconds() {
        val currentValue = binding.seconds.text.toString().toInt()
        if (currentValue > 0) {
            binding.seconds.text = (currentValue - 1).toString()
        }
    }

}
