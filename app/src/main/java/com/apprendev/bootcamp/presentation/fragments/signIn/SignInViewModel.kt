package com.apprendev.bootcamp.presentation.fragments.signIn

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.apprendev.bootcamp.domain.EmailHasBeenChangedUseCase
import com.apprendev.bootcamp.domain.PasswordHasBeenChangedUseCase
import com.apprendev.bootcamp.entities.FormInputState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SignInViewModel @Inject constructor(
    private val emailHasBeenChangedUseCase: EmailHasBeenChangedUseCase,
    private val passwordHasBeenChangedUseCase: PasswordHasBeenChangedUseCase
) : ViewModel() {

    val signInState: MutableLiveData<SignInUIState> =
        MutableLiveData(SignInUIState.SignInFormIdle)

    private val scope =
        CoroutineScope(Job() + Dispatchers.Main)

    private var emailSignInForm: FormInputState? = null
    private var passwordSignInForm: FormInputState? = null

    fun emailHasBeenChanged(input: String) {
        scope.launch {
            emailSignInForm = emailHasBeenChangedUseCase.invoke(input)
        }
    }

    fun passwordHasBeenChanged(input: String) {
        scope.launch {
            passwordSignInForm = passwordHasBeenChangedUseCase.invoke(input)
        }
    }
}


