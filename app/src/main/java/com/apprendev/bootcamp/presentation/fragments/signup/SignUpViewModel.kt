package com.apprendev.bootcamp.presentation.fragments.signup

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.apprendev.bootcamp.data.database.UserDao
import com.apprendev.bootcamp.data.database.entities.UserEntity
import com.apprendev.bootcamp.domain.EmailValidator
import com.apprendev.bootcamp.domain.NameValidator
import com.apprendev.bootcamp.domain.PasswordValidator
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SignUpViewModel @Inject constructor(
    private val usersDao: UserDao,
    private val nameValidator: NameValidator,
    private val lastNameValidator: NameValidator,
    private val emailValidator: EmailValidator,
    private val passwordValidator: PasswordValidator,
    private val formValidatorUseCase: SignUpFormValidatorUseCase,
    private val signUpFormApi: SignUpFormPersistence
) : ViewModel() {

    private val scope =
        CoroutineScope(Job() + Dispatchers.Main)

    val state = MutableLiveData<SignUpFormState>(SignUpFormState.SignUpFormIdle)

    fun nameHasBeenChanged(name: String) {
        scope.launch {
            val nameValid = nameValidator.invoke(name)
            val signUpForm = signUpFormApi.getSignUpForm()
            if (nameValid) {
                signUpForm.name = name
            } else {
                signUpForm.name = null
            }
            signUpFormApi.updateSignUpForm(signUpForm)
            validateForm()
        }
    }


    fun lastNameHasBeenChanged(lastName: String) {
        scope.launch {
            val lastNameValid = lastNameValidator.invoke(lastName)
            val signUpForm = signUpFormApi.getSignUpForm()
            if (lastNameValid) {
                signUpForm.lastName = lastName
            } else {
                signUpForm.lastName = null
            }
            signUpFormApi.updateSignUpForm(signUpForm)
            validateForm()
        }
    }

    fun emailHasBeenChanged(email: String) {
        scope.launch {
            val emailValid = emailValidator.invoke(email)
            val signUpForm = signUpFormApi.getSignUpForm()
            if (emailValid) {
                signUpForm.email = email
            } else {
                signUpForm.email = null
            }
            signUpFormApi.updateSignUpForm(signUpForm)
            validateForm()
        }
    }

    fun passwordHasBeenChanged(password: String) {
        scope.launch {
            val passwordValid = passwordValidator.invoke(password)
            val signUpForm = signUpFormApi.getSignUpForm()
            if (passwordValid) {
                signUpForm.password = password
            } else {
                signUpForm.password = null
            }
            signUpFormApi.updateSignUpForm(signUpForm)
            validateForm()
        }
    }

    private fun validateForm() {
        scope.launch {
            try {
                val isFormReady = formValidatorUseCase.invoke()
                if (isFormReady) {
                    state.postValue(SignUpFormState.SignUpFormComplete)
                } else {
                    state.postValue(SignUpFormState.SignUpFormIncomplete)
                }
            } catch (e: Exception) {
                state.postValue(SignUpFormState.SignUpFormError(e))
            }
        }
    }

    fun signUp() {
        scope.launch {
            val form = signUpFormApi.getSignUpForm()
            val isFormReady = formValidatorUseCase.invoke()
            if (isFormReady) {
                scope.launch {
                    val user = UserEntity(
                        firstName = form.name!!,
                        lastName = form.lastName!!,
                        password = form.password!!,
                        email = form.email!!
                    )
                    usersDao.insertAll(user)
                    signUpFormApi.updateSignUpForm(SignUpForm())
                    state.postValue(SignUpFormState.SignUpFormSubmitted)
                }
            } else {
                println("Seems that the form is corrupted.")
            }
        }
    }
}


