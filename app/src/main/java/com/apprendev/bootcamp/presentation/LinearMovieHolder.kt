package com.apprendev.bootcamp.presentation

import android.os.Bundle
import androidx.navigation.findNavController
import coil.load
import com.apprendev.bootcamp.R
import com.apprendev.bootcamp.data.database.entities.MovieEntity
import com.apprendev.bootcamp.databinding.LinearMovieHolderBinding
import com.apprendev.bootcamp.presentation.fragments.MOVIE_ID
import com.apprendev.bootcamp.presentation.fragments.favorites.FavoritesContract
import com.apprendev.bootcamp.presentation.viewmodels.BaseListViewModel


class LinearMovieHolder(
    binding: LinearMovieHolderBinding,
    private val viewModel: BaseListViewModel?
) :
    BaseMovieHolder<LinearMovieHolderBinding>(binding) {

    override fun bind(movie: MovieEntity) {

        with(binding) {
            movieName.text = movie.title
            movieImage.load(movie.backdrop_path)

            popularity.text = "Popularity: ${movie.popularity}"

            seeMovieDetails.setOnClickListener {
                it.findNavController()
                    .navigate(
                        R.id.action_popularMovies_to_movieDetails,
                        Bundle().apply {
                            putString(MOVIE_ID, movie.id)
                        }
                    )
            }

            updateFavoriteUI(movie)

            binding.favorite.setOnClickListener {
                if (movie.isFavorite) {
                    (viewModel as? FavoritesContract)?.deleteFavoriteMovie(movie)
                } else {
                    (viewModel as? FavoritesContract)?.addFavoriteMovie(movie)
                }
                movie.isFavorite = movie.isFavorite.not()
                updateFavoriteUI(movie)
            }
        }
    }

    private fun updateFavoriteUI(movie: MovieEntity) {
        if (movie.isFavorite) {
            binding.favorite.load(R.drawable.ic_favorite_applied)
        } else {
            binding.favorite.load(R.drawable.ic_favorite)
        }
    }
}

