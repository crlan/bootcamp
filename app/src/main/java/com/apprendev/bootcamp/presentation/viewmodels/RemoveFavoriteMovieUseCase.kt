package com.apprendev.bootcamp.presentation.viewmodels

import com.apprendev.bootcamp.data.database.MoviesDao
import com.apprendev.bootcamp.data.database.entities.MovieEntity
import com.apprendev.bootcamp.domain.FAVORITES_MOVIES_LIST
import com.apprendev.bootcamp.domain.POPULAR_MOVIES_LIST
import javax.inject.Inject

class RemoveFavoriteMovieUseCase @Inject constructor(private val moviesDao: MoviesDao) {

    suspend fun invoke(movie: MovieEntity) {

        val movieInPopularList = moviesDao.findMovieByTitleInList(
            listName = POPULAR_MOVIES_LIST,
            movieTitle = movie.title
        )

        if (movieInPopularList != null) {
            moviesDao.updateUserMovie(
                movieInPopularList.copy(isFavorite = false)
            )
        }

        val movieInFavoriteList = moviesDao.findMovieByTitleInList(
            listName = FAVORITES_MOVIES_LIST,
            movieTitle = movie.title
        )

        if (movieInFavoriteList != null) {
            moviesDao.deleteUserMovie(movieInFavoriteList)
        }
    }
}
