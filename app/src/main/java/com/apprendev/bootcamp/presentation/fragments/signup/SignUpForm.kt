package com.apprendev.bootcamp.presentation.fragments.signup

data class SignUpForm(
    var name: String? = null,
    var lastName: String? = null,
    var email: String? = null,
    var password: String? = null
)
