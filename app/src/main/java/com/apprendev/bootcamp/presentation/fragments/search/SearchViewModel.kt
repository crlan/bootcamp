package com.apprendev.bootcamp.presentation.fragments.search

import com.apprendev.bootcamp.domain.SearchForUseCase
import com.apprendev.bootcamp.presentation.state.MoviesState
import com.apprendev.bootcamp.presentation.viewmodels.BaseListViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SearchViewModel @Inject constructor(
    private val searchForUseCase: SearchForUseCase
) : BaseListViewModel() {

    fun searchFor(query: String) {
        scope.launch {
            val result = searchForUseCase.invoke(query)
            state.postValue(MoviesState.MoviesReady(result))
        }
    }
}
