package com.apprendev.bootcamp.utils

import kotlin.math.roundToInt

class Calculadora(val name: String) {

    /**
     * sumar ambos numeros
     */
    fun initTimmer(
        objA: Int,
        objB: Int,
        isSuma: Boolean
    ): Int {
        if (isSuma) {
            return objA + objB

        }

        return objA - objB
    }

    fun suma(numeros: List<Int>): Int {

        var miLista = ArrayList<Int>()
        miLista.add(12)
        miLista.add(12)
        miLista.add(12)
        miLista.add(12)
        miLista.add(12)

        var miListaConKotlin = listOf<Int>(12, 3, 1, 2, 1)

        val alumnos =
            hashMapOf<String, String>(Pair("alan", "2"), Pair("giovani", "8"), Pair("aaa", "8"))

        val calificacion = alumnos["xxxx"]

        calificacion?.let {
            println("El alumno obtuvo $it de calificación")
        }


        if (calificacion == null) {
            print("El alumno no existe")
        }


        val items: List<String> = listOf("apple", "banana", "kiwifruit")
        for (item in items) {
            // 1a item = "apple"
            // 2a item = "banana"
            // 2a item = "kiwifruit"
            println(item)
        }

        val numero: Float = 12.5F
        val numeroAsString = "123".toInt()

        var otroNumero: Int = numero.roundToInt()

        return 0
    }

    fun resta(
        objA: Int,
        objB: Int
    ): Int {
        return objA - objB
    }

    fun multiplicacion(
        objA: Int,
        objB: Int
    ): Int {
        return objA * objB
    }

    fun division(
        objA: Int,
        objB: Int
    ): Int {
        return objA * objB
    }
}


