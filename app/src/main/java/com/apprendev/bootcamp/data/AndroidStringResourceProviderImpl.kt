package com.apprendev.bootcamp.data

import android.content.Context
import androidx.annotation.StringRes
import com.apprendev.bootcamp.domain.AndroidStringResourceProvider


class AndroidStringResourceProviderImpl(private val context: Context) :
    AndroidStringResourceProvider {

    override fun getString(@StringRes identifier: Int): String {
        return context.getString(identifier)
    }
}
