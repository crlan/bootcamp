package com.apprendev.bootcamp.data.configuration

data class ApiConfiguration(
    val change_keys: List<String>,
    val images: Images
)
