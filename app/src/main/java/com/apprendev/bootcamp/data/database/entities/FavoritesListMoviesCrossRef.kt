package com.apprendev.bootcamp.data.database.entities

import androidx.room.Entity

@Entity(primaryKeys = ["listName", "movieId"])
data class FavoritesListMoviesCrossRef(
    val listName: String,
    val movieId: String
)
