package com.apprendev.bootcamp.data.remoteconfig

data class FirebaseMenu(
  val menu: List<Menu>
)
