package com.apprendev.bootcamp.data.popularmovies

data class Movie(
    val original_title: String?,
    val overview: String?,
    val title: String?,
    val vote_average: String?,
    val popularity: String?,
    val release_date: String?,
    val poster_path: String?,
    val backdrop_path: String?,
    var imageUrl: String = "",
    val id: String?
)
