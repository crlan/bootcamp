package com.apprendev.bootcamp.data.details

data class Genre(
    val id: Int,
    val name: String
)
