package com.apprendev.bootcamp.data.database.entities

import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation

data class ListWithMovies(
    @Embedded val list: MoviesList,
    @Relation(
        parentColumn = "listName",
        entityColumn = "movieId",
        associateBy = Junction(FavoritesListMoviesCrossRef::class)
    )
    val movies: List<MovieEntity>
)
