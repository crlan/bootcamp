package com.apprendev.bootcamp.data.popularmovies

import com.apprendev.bootcamp.data.configuration.ApiConfiguration
import com.apprendev.bootcamp.data.details.MovieDetails
import com.apprendev.bootcamp.data.search.SearchEntity
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface TheMovieDbServices {
    @GET("movie/popular?api_key=076ecccc1c181908a50db5448faa1ec4")
    suspend fun getPopularMovies(@Query("page") page: Int = 1): PopularMovies

    @GET("movie/latest?api_key=076ecccc1c181908a50db5448faa1ec4")
    suspend fun getLatestMovie(): MovieDetails

    @GET("configuration?api_key=076ecccc1c181908a50db5448faa1ec4")
    suspend fun getApiConfiguration(): ApiConfiguration

    @GET("movie/{movie_id}?api_key=076ecccc1c181908a50db5448faa1ec4")
    suspend fun getMovieDetails(@Path("movie_id") movieId: String): MovieDetails

    @GET("search/movie?api_key=076ecccc1c181908a50db5448faa1ec4")
    suspend fun searchInMovieApi(@Query("query") query: String): SearchEntity
}


