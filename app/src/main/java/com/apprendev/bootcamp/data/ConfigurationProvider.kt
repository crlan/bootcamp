package com.apprendev.bootcamp.data


interface ConfigurationProvider {
    var configurationFetchedCallback: ConfigurationHasBeenFetched?

    fun <T : Any> getConfigurationValue(id: String, type: Class<T>): T

    interface ConfigurationHasBeenFetched {
        fun completed()
    }
}

