package com.apprendev.bootcamp.data.search

import com.apprendev.bootcamp.data.popularmovies.Movie


data class SearchEntity(
    val page: Int,
    val results: List<Movie>
)
