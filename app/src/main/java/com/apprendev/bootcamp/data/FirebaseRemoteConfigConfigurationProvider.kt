package com.apprendev.bootcamp.data

import android.app.Activity
import com.apprendev.bootcamp.data.remoteconfig.FirebaseMenu
import com.google.firebase.ktx.Firebase
import com.google.firebase.remoteconfig.ktx.remoteConfig
import com.google.gson.Gson

class FirebaseRemoteConfigConfigurationProvider(
    private val activity: Activity,
    override var configurationFetchedCallback: ConfigurationProvider.ConfigurationHasBeenFetched?
) :
    ConfigurationProvider {
    private var remoteConfigCompleted: Boolean = false
    private val remoteConfig = Firebase.remoteConfig

    init {
        fetchRemoteConfig()
    }

    private fun fetchRemoteConfig() {
        remoteConfig
            .fetchAndActivate()
            .addOnCompleteListener(activity) {
                if (it.isSuccessful) {
                    remoteConfigCompleted = true
                    configurationFetchedCallback?.completed()
                }
            }
    }

    override fun <T : Any> getConfigurationValue(id: String, type: Class<T>): T {
        val payload = remoteConfig.getString(id)
        return when (type) {
            FirebaseMenu::class.java -> {
                return Gson().fromJson(payload, type) as T
            }
            else -> remoteConfig.getString(id) as T
        }
    }

}
