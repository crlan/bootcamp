package com.apprendev.bootcamp.data.popularmovies


data class PopularMovies(
    val page: Int,
    val results: List<Movie>,
    val totalPages: Int
)


