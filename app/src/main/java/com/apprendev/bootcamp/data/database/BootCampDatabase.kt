package com.apprendev.bootcamp.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.apprendev.bootcamp.data.database.entities.MovieEntity
import com.apprendev.bootcamp.data.database.entities.MoviesList
import com.apprendev.bootcamp.data.database.entities.UserEntity

@Database(entities = [UserEntity::class, MovieEntity::class, MoviesList::class], version = 7)
abstract class BootCampDatabase : RoomDatabase() {
    abstract fun userDao(): UserDao

    abstract fun moviesDao(): MoviesDao
}
