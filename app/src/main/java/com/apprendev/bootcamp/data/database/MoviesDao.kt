package com.apprendev.bootcamp.data.database

import androidx.room.*
import androidx.room.OnConflictStrategy.REPLACE
import com.apprendev.bootcamp.data.database.entities.MovieEntity
import com.apprendev.bootcamp.data.database.entities.MoviesList
import com.apprendev.bootcamp.domain.FAVORITES_MOVIES_LIST
import kotlinx.coroutines.flow.Flow

@Dao
interface MoviesDao {
    @Insert
    suspend fun insertUserMovie(vararg movie: MovieEntity)

    @Delete
    suspend fun deleteUserMovie(movie: MovieEntity)

    @Update
    suspend fun updateUserMovie(movie: MovieEntity)

    @Transaction
    @Query("SELECT * FROM MovieEntity")
    fun getUserMovies(): Flow<List<MovieEntity>>

    @Insert
    suspend fun insertList(vararg moviesList: MoviesList)

    @Delete
    suspend fun deleteList(moviesList: MoviesList)

    @Query("SELECT * FROM MovieEntity WHERE listName = :listName AND title = :movieTitle")
    suspend fun findMovieByTitleInList(movieTitle: String, listName: String): MovieEntity?

    @Query("SELECT * FROM MoviesList")
    fun getMoviesLists(): List<MoviesList>

    @Transaction
    @Query("SELECT * FROM MovieEntity WHERE listName = :listName ORDER BY popularity DESC")
    fun getMoviesByListName(listName: String): Flow<List<MovieEntity>>

    @Transaction
    @Query("SELECT * FROM MovieEntity WHERE listName = :listName ORDER BY popularity DESC")
    suspend fun getFavoritesMovies(listName: String = FAVORITES_MOVIES_LIST): List<MovieEntity>

    @Query("SELECT * FROM MoviesList WHERE listName = :listName")
    fun getListByName(listName: String): MoviesList?
}
