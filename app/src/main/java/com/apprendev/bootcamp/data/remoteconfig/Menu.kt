package com.apprendev.bootcamp.data.remoteconfig

data class Menu(
  val menuColor: String,
  val menuTitle: String,
  val animation: String
)
