package com.apprendev.bootcamp.data.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class MoviesList(
    @PrimaryKey
    @ColumnInfo(name = "listName") val listName: String,
)
