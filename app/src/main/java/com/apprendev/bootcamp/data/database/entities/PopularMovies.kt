package com.apprendev.bootcamp.data.database.entities

import androidx.room.Embedded
import androidx.room.Relation

data class PopularMovies(
    @Embedded val list: MoviesList,

    @Relation(
        parentColumn = "listName",
        entityColumn = "movieId"
    )
    val movies: List<MovieEntity>
)


