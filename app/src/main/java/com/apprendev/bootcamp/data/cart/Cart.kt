package com.apprendev.bootcamp.data.cart

data class Cart(
  val cartTotal: Double,
  val cartTotalQuantity: Int,
  val id: String,
  val lineItemCount: Int,
  val restrictedItemTypes: List<Any>,
  val volume: Int,
  val weight: Int
)
