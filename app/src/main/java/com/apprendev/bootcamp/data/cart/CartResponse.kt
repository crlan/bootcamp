package com.apprendev.bootcamp.data.cart

data class CartResponse(
  val cart: Cart,
  val errors: List<Any>
)
