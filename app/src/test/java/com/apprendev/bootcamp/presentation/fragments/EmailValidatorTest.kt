package com.apprendev.bootcamp.presentation.fragments

import com.apprendev.bootcamp.domain.EmailValidator
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Rule
import org.junit.Test


class EmailValidatorTest {

    @ExperimentalCoroutinesApi
    @get:Rule
    val coroutineRule = MainCoroutineRule()

    @ExperimentalCoroutinesApi
    @Test
    fun `an correct email`() = coroutineRule.runBlockingTest {
        // GIVEN
        val validator = EmailValidator()

        // WHEN
        val email = "correo@correo.com"
        val result = validator.invoke(email)

        // THEN
        Assert.assertTrue(
            "El email usado como argumento de entrada no es un email valido. [$email]",
            result
        )
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `an invalid email`() = coroutineRule.runBlockingTest {
        // GIVEN
        val validator = EmailValidator()

        // WHEN
        val email = "correo@.com"
        val result = validator.invoke(email)

        // THEN
        Assert.assertFalse(
            "El email usado como argumento de entrada no es un email valido. [$email]",
            result
        )
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `one invalid character`() = coroutineRule.runBlockingTest {
        // GIVEN
        val validator = EmailValidator()
        // WHEN
        val email = "correo@correo/.com"
        val result = validator.invoke(email)
        // THEN
        Assert.assertFalse(
            "El email usado como argumento de entrada no es un email valido. [$email]",
            result
        )
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `invalid email with some spaces at the beginning`() = coroutineRule.runBlockingTest {
        // GIVEN
        val validator = EmailValidator()
        // WHEN
        val email = " correo@correo.com"
        val result = validator.invoke(email)
        // THEN
        Assert.assertFalse(
            "El email usado como argumento de entrada no es un email valido. [$email]",
            result
        )
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `invalid email with some spaces at the end`() = coroutineRule.runBlockingTest {
        // GIVEN
        val validator = EmailValidator()
        // WHEN
        val email = "correo@correo.com "
        val result = validator.invoke(email)
        // THEN
        Assert.assertFalse(
            "El email usado como argumento de entrada no es un email valido. [$email]",
            result
        )
    }

}
