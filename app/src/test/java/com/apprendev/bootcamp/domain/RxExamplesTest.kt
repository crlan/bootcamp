package com.apprendev.bootcamp.domain

import io.reactivex.rxjava3.subjects.*
import org.junit.Assert
import org.junit.Test


class RxExamplesTest {


    @Test
    fun single_subject() {
        val singleSubject: SingleSubject<String> = SingleSubject.create()
        val data = "datos"

        val result = singleSubject.test()

        val values = result.values()

        Assert.assertTrue(
            "Expected zero size however was ${values.size}",
            values.size == 0
        )

        // Manual emision
        singleSubject.onSuccess(data)

        val valuesAfterOnSuccess = result.values()

        Assert.assertTrue(
            "Expected one value however was ${valuesAfterOnSuccess.size}",
            valuesAfterOnSuccess.size > 0
        )
    }


    @Test
    fun publish_subject() {
        val subject: PublishSubject<String> = PublishSubject.create()
        subject.onNext("value 0")
        subject.onNext("value 1")
        subject.onNext("value 2")

        val result = subject.test()
        subject.onNext("value 3")
        subject.onNext("value 4")
        subject.onNext("value 5")

        val values = result.values()

        Assert.assertTrue(
            "Expected size of 3 however was ${values.size}",
            values.size == 3
        )
    }


    @Test
    fun behavior_subject() {
        val subject: BehaviorSubject<String> = BehaviorSubject.create()
        subject.onNext("value 0")
        subject.onNext("value 1")
        subject.onNext("value 2")

        val result = subject.test()
        subject.onNext("value 3")
        subject.onNext("value 4")
        subject.onNext("value 5")

        val values = result.values()

        Assert.assertTrue(
            "Expected size of 4 however was ${values.size}",
            values.size == 4
        )
    }


    @Test
    fun replay_subject() {
        val subject: ReplaySubject<String> = ReplaySubject.create()
        subject.onNext("value 0")
        subject.onNext("value 1")
        subject.onNext("value 2")


        subject.onNext("value 3")
        subject.onNext("value 4")
        subject.onNext("value 5")

        val result = subject.test()
        val values = result.values()

        Assert.assertTrue(
            "Expected size of 6 however was ${values.size}",
            values.size == 6
        )
    }


    @Test
    fun merge_operator_same_data_type() {
        val subjectA: PublishSubject<Int> = PublishSubject.create()
        val subjectB: PublishSubject<Int> = PublishSubject.create()


        val subjectsMerged = subjectA
            .mergeWith(subjectB)
            .filter {
                it > 1
            }
            .map {
                it * 10
            }
            .map {
                it - 5
            }


        val result = subjectsMerged.test()
        subjectB.onNext(1)
        subjectA.onNext(100)
        subjectA.onNext(80)
        subjectB.onNext(1)
        subjectA.onNext(60)


        val values = result.values()

        Assert.assertTrue(
            "Expected size of 3 however was ${values.size}",
            values.size == 3
        )
    }


    fun example() {
        val subject1: SingleSubject<String> = SingleSubject.create()

        val publishSubject: PublishSubject<String> = PublishSubject.create()
        val behaviorSubject: BehaviorSubject<String> = BehaviorSubject.create()

        val replaySubject: ReplaySubject<String> = ReplaySubject.create()
        val completableSubject: CompletableSubject = CompletableSubject.create()

        val data: String = "data"

        publishSubject.onNext(data)
        publishSubject.onNext(data)
        publishSubject.onNext(data)
        publishSubject.onNext(data)

        // init subscription
        publishSubject.onNext(data)
        publishSubject.onNext(data)
        publishSubject.onNext(data)


        subject1.onSuccess(data)
    }
}
