package com.example.example

import com.apprendev.bootcamp.utils.Calculadora
import org.junit.Assert
import org.junit.Test

class CalculadoraTest {

    @Test
    fun validarSuma() {
        val calculadora: Calculadora =
            Calculadora("mi calculadora")
        val resultado = calculadora.suma(listOf(2, 3))

        Assert
            .assertTrue(
                "Se esperaba un resultado 5, sin embargo se obtuvo [$resultado]",
                resultado == 5
            )
    }

    @Test
    fun validarSumaLista() {
        val calculadora: Calculadora =
            Calculadora("mi calculadora")
        val resultado = calculadora.suma(listOf(10, 20, 30, 40, 50))

        Assert
            .assertTrue(
                "Se esperaba un resultado 5, sin embargo se obtuvo [$resultado]",
                resultado == 5
            )
    }

    @Test
    fun validarResta() {
        val calculadora: Calculadora =
            Calculadora("mi calculadora")
        val resultado = calculadora.resta(3, 1)

        Assert.assertTrue(
            "Se esperaba un resultado 2, sin embargo se obtuvo [$resultado]",
            resultado == 2
        )
    }

}
